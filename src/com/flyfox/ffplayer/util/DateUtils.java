package com.flyfox.ffplayer.util;

import javafx.util.Duration;

public class DateUtils {

	public static String formatTime(Duration duration) {
		int totalSec = (int) Math.floor(duration.toSeconds());
		int hours = totalSec / (60 * 60);
		if (hours > 0) {
			totalSec -= hours * 60 * 60;
		}
		int min = totalSec / 60;
		int sec = totalSec - hours * 60 * 60 - min * 60;

		if (hours > 0) {
			return String.format("%d:%02d:%02d", hours, min, sec);
		} else {
			return String.format("%02d:%02d", min, sec);
		}
	}
	
}
