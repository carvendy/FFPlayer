package com.flyfox.ffplayer.util;

import java.io.RandomAccessFile;

/**
 * Mp3信息获取
 * 
 * @author flyfox
 * @date 2014年11月7日
 */
public class MP3Utils {

	public static void main(String[] args) throws Exception {
		String path = MP3Utils.class.getResource("/").getFile() //
				+ "/resources/e.mp3";
		init(path);
	}

	public static void init(String path) throws Exception {
		byte[] buf = new byte[128];// 初始化标签信息的byte数组
		RandomAccessFile raf = new RandomAccessFile(path, "r");// 随机读写方式打开MP3文件
		raf.seek(raf.length() - 128);// 移动到文件MP3末尾
		raf.read(buf);// 读取标签信息
		raf.close();// 关闭文件
		if (buf.length != 128) {// 数据长度是否合法
			throw new Exception("MP3标签信息数据长度不合法!");
		}

		if (!"TAG".equalsIgnoreCase(new String(buf, 0, 3))) {// 标签头是否存在
			throw new Exception("MP3标签信息数据格式不正确!");
		}

		String SongName = new String(buf, 3, 30).trim();// 歌曲名称
		String Artist = new String(buf, 33, 30).trim();// 歌手名字
		String Album = new String(buf, 63, 30).trim();// 专辑名称
		String aa = new String(buf, 125, 1).trim();// 专辑名称
		System.out.println(SongName + ":" + Artist + ":" + Album + ":" + aa);

	}
}
