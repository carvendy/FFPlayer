package com.flyfox.ffplayer.control;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;

import com.flyfox.ffplayer.FFPlayerManager;
import com.flyfox.ffplayer.model.FFPlayer;
import com.flyfox.ffplayer.util.FFPlayerUtils;
import com.flyfox.ffplayer.util.ImageUtils;

public class MainControl {

	private static final double HEIGHT = 600;
	private static final double WIGHT = 1000;
	FFPlayerManager ffPlayer;
	Stage stage;

	private ParallelTransition transition = null;
	Pane bottom;
	TableView<FFPlayer> musicList;

	public MainControl(Stage stage) {
		this.stage = stage;
	}

	public void init() {
		// 初始化
		ffPlayer = new FFPlayerManager(stage);
		bottom = ffPlayer.getFuncBottom().getNode();
		musicList = ffPlayer.getMusicList().getNode();

		Scene scene = new Scene(new BorderPane(), WIGHT, HEIGHT);

		init((BorderPane) scene.getRoot());

		stage.setFullScreen(false);
		stage.setResizable(false);
		stage.setTitle("FFPlayer");
		stage.setScene(scene);
		// stage.show();

		stage.setOnCloseRequest(event -> {
			while (!FFPlayerUtils.CLOSE.get()) {
				try {
					Thread.sleep(100L);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			Platform.exit();
			System.exit(0);
		});
	}

	private void init(BorderPane root) {
		final HBox vbox = new HBox();
		vbox.setSpacing(5);
		Pane info = ffPlayer.getLyricInfo().getNode();

		vbox.getChildren().addAll(musicList, info);
		root.setCenter(vbox);
		root.setBottom(bottom);

		// 加入背景
		BackgroundImage bgImage = new BackgroundImage(ImageUtils.getImage("bg.jpg"), BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
		Background bg = new Background(bgImage);
		root.setBackground(bg);

		// 划入显示菜单
		root.setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				if (transition != null)
					transition.stop();
				// 底部功能区
				FadeTransition fade = new FadeTransition();
				fade.setNode(bottom);
				fade.setDuration(Duration.millis(1000));
				fade.setToValue(1);
				fade.setInterpolator(Interpolator.EASE_OUT);

				// 播放列表
				FadeTransition fadeMusicList = new FadeTransition();
				fadeMusicList.setNode(musicList);
				fadeMusicList.setDuration(Duration.millis(1000));
				fadeMusicList.setToValue(1);
				fadeMusicList.setInterpolator(Interpolator.EASE_OUT);

				transition = new ParallelTransition(fade, fadeMusicList);
				transition.play();
			}
		});

		// 划出不显示菜单
		root.setOnMouseExited(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				if (transition != null)
					transition.stop();
				FadeTransition fade = new FadeTransition();
				fade.setNode(bottom);
				fade.setDuration(Duration.millis(1000));
				fade.setToValue(0);
				fade.setInterpolator(Interpolator.EASE_OUT);

				// 播放列表
				FadeTransition fadeMusicList = new FadeTransition();
				fadeMusicList.setNode(musicList);
				fadeMusicList.setDuration(Duration.millis(1000));
				fadeMusicList.setToValue(0);
				fadeMusicList.setInterpolator(Interpolator.EASE_OUT);

				transition = new ParallelTransition(fade, fadeMusicList);
				transition.play();
			}
		});

		// 拖拽需要显示出播放列表
		root.setOnDragOver(event -> {
			if (transition != null)
				transition.stop();
			// 播放列表
			FadeTransition fadeMusicList = new FadeTransition();
			fadeMusicList.setNode(musicList);
			fadeMusicList.setDuration(Duration.millis(500));
			fadeMusicList.setToValue(1);
			fadeMusicList.setInterpolator(Interpolator.EASE_OUT);

			transition = new ParallelTransition(fadeMusicList);
			transition.play();
		});
	}
}
