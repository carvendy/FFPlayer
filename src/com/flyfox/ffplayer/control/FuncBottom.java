package com.flyfox.ffplayer.control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Lighting;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import com.flyfox.ffplayer.model.PlayMode;
import com.flyfox.ffplayer.util.EventUtils;
import com.flyfox.ffplayer.util.ImageUtils;

/**
 * 底部，功能按钮
 * 
 * @author flyfox
 * @date 2014年11月6日
 */
public class FuncBottom {

	Label play; // 播放，暂停
	Label pre; // 上一首
	Label next; // 下一首

	Label name; // 名称展示
	final Slider timeSlider = new Slider(); // 时间滑块
	Label timeLabel; // 时间显示

	ChoiceBox<String> playModelChoiceBox;

	Label volumeLabel; // 音量
	final Slider volumeSlider = new Slider(); // 音量滑块

	public FuncBottom() {
		init();
	}

	public void init() {
		initPlayButton();
		initTime();
		initPlayMode();
		initVolume();
	}

	public HBox getNode() {
		HBox bottomPane = new HBox();
		
		bottomPane.setPrefHeight(60);
		bottomPane.setMaxHeight(60);
		bottomPane.setMinHeight(60);
		
		bottomPane.getChildren().add(pre);
		bottomPane.getChildren().add(play);
		bottomPane.getChildren().add(next);

		HBox infoBox = new HBox();
		HBox timeBox = new HBox();
		VBox centerBox = new VBox();
		infoBox.getChildren().add(name);
		timeBox.getChildren().add(timeSlider);
		timeBox.getChildren().add(timeLabel);
		centerBox.getChildren().add(infoBox);
		centerBox.getChildren().add(timeBox);
		Color bottomPaint = Color.gray(0, 0.1);
		Background bottomBg = new Background(new BackgroundFill(bottomPaint, CornerRadii.EMPTY, new Insets(0)));
		centerBox.setBackground(bottomBg);

		bottomPane.getChildren().add(centerBox);

		VBox playModelBox = new VBox();
		playModelBox.getChildren().add(playModelChoiceBox);
		playModelBox.setPadding(new Insets(15, 0, 0, 10));
		bottomPane.getChildren().add(playModelBox);

		bottomPane.getChildren().add(volumeLabel);
		bottomPane.getChildren().add(volumeSlider);

		Color paint = Color.gray(0, 0.2);
		Background bg = new Background(new BackgroundFill(paint, CornerRadii.EMPTY, new Insets(1)));
		bottomPane.setBackground(bg);

		return bottomPane;
	}

	/**
	 * 加入按钮，播放，上一首，下一首
	 * 
	 * @param bottomPane
	 */
	private void initPlayButton() {
		// 播放 暂停
		Tooltip pauseTooltip = new Tooltip("暂停");
		Tooltip preTooltip = new Tooltip("上一首");
		Tooltip nextTooltip = new Tooltip("下一首");

		ImageView playIamgeView = ImageUtils.getImageView("pause.png", 48, 48);
		play = new Label("", playIamgeView);
		play.setPadding(new Insets(0, 0, 0, 0));
		play.setTooltip(pauseTooltip);
		play.setCursor(Cursor.HAND);
		EventUtils.mouseOn(play, new Lighting());

		// 上一首
		ImageView preIamgeView = ImageUtils.getImageView("pre.png", 40, 40);
		pre = new Label("", preIamgeView);
		pre.setPadding(new Insets(5, 5, 5, 5));
		pre.setTooltip(preTooltip);
		pre.setCursor(Cursor.HAND);
		EventUtils.mouseOn(pre, new Lighting());

		// 下一首
		ImageView nextIamgeView = ImageUtils.getImageView("next.png", 40, 40);
		next = new Label("", nextIamgeView);
		next.setPadding(new Insets(5, 5, 5, 5));
		next.setTooltip(nextTooltip);
		next.setCursor(Cursor.HAND);
		EventUtils.mouseOn(next, new Lighting());

	}

	/**
	 * 时间
	 */
	private void initTime() {
		name = new Label();
		name.setPadding(new Insets(5, 5, 5, 30));
		name.setFont(Font.font(null, FontWeight.BOLD, 18));
		name.setTextFill(Color.WHITE);

		timeSlider.setPadding(new Insets(5, 5, 5, 30));
		timeSlider.setMin(0.0);
		timeSlider.setMax(1.0);
		timeSlider.setValue(0.0);
		timeSlider.setPrefWidth(500);
		timeSlider.setMaxWidth(Region.USE_PREF_SIZE);
		timeSlider.setMinWidth(30);

		timeLabel = new Label("00:00/00:00");
		timeLabel.setPadding(new Insets(5, 5, 5, 5));
		timeLabel.setTextFill(Color.WHITE);

	}

	/**
	 * 播放模式
	 */
	private void initPlayMode() {
		ObservableList<String> list = FXCollections.observableArrayList(PlayMode.CYCLING.getName(),
				PlayMode.RANDOM.getName(), PlayMode.ORDER.getName(), PlayMode.ONE_CYCLING.getName());
		playModelChoiceBox = new ChoiceBox<String>();
		playModelChoiceBox.setItems(list);
		// 默认单曲循环
		playModelChoiceBox.setValue(PlayMode.ONE_CYCLING.getName());
		playModelChoiceBox.setPadding(new Insets(0, 5, 5, 20));
	}

	/**
	 * 声音
	 */
	private void initVolume() {
		volumeLabel = new Label("音量");
		volumeLabel.setPadding(new Insets(20, 5, 5, 20));
		volumeLabel.setTextFill(Color.WHITE);

		volumeSlider.setPadding(new Insets(20, 5, 5, 5));
		volumeSlider.setMin(0.0);
		volumeSlider.setMax(1.0);
		volumeSlider.setValue(1.0);
		volumeSlider.setPrefWidth(100);
		volumeSlider.setMaxWidth(Region.USE_PREF_SIZE);
		volumeSlider.setMinWidth(30);

	}

	public Label getPlay() {
		return play;
	}

	public Label getPre() {
		return pre;
	}

	public Label getNext() {
		return next;
	}

	public Label getName() {
		return name;
	}

	public Slider getTimeSlider() {
		return timeSlider;
	}

	public Label getTimeLabel() {
		return timeLabel;
	}

	public Label getVolumeLabel() {
		return volumeLabel;
	}

	public Slider getVolumeSlider() {
		return volumeSlider;
	}

	public ChoiceBox<String> getPlayModelChoiceBox() {
		return playModelChoiceBox;
	}

}
