package com.flyfox.ffplayer;

import javafx.application.Application;
import javafx.stage.Stage;

import com.flyfox.ffplayer.control.MainControl;

public class MainFFPlayer extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		MainControl main = new MainControl(stage);
		main.init();
	}

}
